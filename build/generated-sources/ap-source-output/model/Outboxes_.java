package model;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2019-09-13T19:30:25")
@StaticMetamodel(Outboxes.class)
public class Outboxes_ { 

    public static volatile SingularAttribute<Outboxes, Date> statusDate;
    public static volatile SingularAttribute<Outboxes, Short> prioritas;
    public static volatile SingularAttribute<Outboxes, String> receiver;
    public static volatile SingularAttribute<Outboxes, String> message;
    public static volatile SingularAttribute<Outboxes, Integer> userId;
    public static volatile SingularAttribute<Outboxes, Integer> transactionId;
    public static volatile SingularAttribute<Outboxes, String> responseCode;
    public static volatile SingularAttribute<Outboxes, String> sender;
    public static volatile SingularAttribute<Outboxes, String> receiverType;
    public static volatile SingularAttribute<Outboxes, Boolean> isCommand;
    public static volatile SingularAttribute<Outboxes, Boolean> sms;
    public static volatile SingularAttribute<Outboxes, Integer> mediaTypeId;
    public static volatile SingularAttribute<Outboxes, Boolean> freeOfCharge;
    public static volatile SingularAttribute<Outboxes, Long> id;
    public static volatile SingularAttribute<Outboxes, Integer> moduleId;
    public static volatile SingularAttribute<Outboxes, Integer> inboxId;
    public static volatile SingularAttribute<Outboxes, Integer> status;
    public static volatile SingularAttribute<Outboxes, Date> createDate;

}