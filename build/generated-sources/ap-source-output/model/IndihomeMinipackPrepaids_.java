package model;

import java.math.BigInteger;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2019-09-13T19:30:25")
@StaticMetamodel(IndihomeMinipackPrepaids.class)
public class IndihomeMinipackPrepaids_ { 

    public static volatile SingularAttribute<IndihomeMinipackPrepaids, Integer> amount;
    public static volatile SingularAttribute<IndihomeMinipackPrepaids, String> produk;
    public static volatile SingularAttribute<IndihomeMinipackPrepaids, String> price;
    public static volatile SingularAttribute<IndihomeMinipackPrepaids, String> preName;
    public static volatile SingularAttribute<IndihomeMinipackPrepaids, String> idpel;
    public static volatile SingularAttribute<IndihomeMinipackPrepaids, Long> id;
    public static volatile SingularAttribute<IndihomeMinipackPrepaids, String> settlementDate;
    public static volatile SingularAttribute<IndihomeMinipackPrepaids, BigInteger> inboxId;
    public static volatile SingularAttribute<IndihomeMinipackPrepaids, String> productType;
    public static volatile SingularAttribute<IndihomeMinipackPrepaids, String> crmName;

}