package model;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2019-09-13T19:30:25")
@StaticMetamodel(Telkoms.class)
public class Telkoms_ { 

    public static volatile SingularAttribute<Telkoms, String> destination;
    public static volatile SingularAttribute<Telkoms, String> ack;
    public static volatile SingularAttribute<Telkoms, String> admin;
    public static volatile SingularAttribute<Telkoms, String> refnbr;
    public static volatile SingularAttribute<Telkoms, String> datel;
    public static volatile SingularAttribute<Telkoms, String> tagihan;
    public static volatile SingularAttribute<Telkoms, String> jmlAdmin;
    public static volatile SingularAttribute<Telkoms, String> xml;
    public static volatile SingularAttribute<Telkoms, Long> id;
    public static volatile SingularAttribute<Telkoms, String> jmlBayar;
    public static volatile SingularAttribute<Telkoms, Date> createDate;
    public static volatile SingularAttribute<Telkoms, String> partnertid;
    public static volatile SingularAttribute<Telkoms, String> pass;
    public static volatile SingularAttribute<Telkoms, Short> reprint;
    public static volatile SingularAttribute<Telkoms, String> transid;
    public static volatile SingularAttribute<Telkoms, String> npwp;
    public static volatile SingularAttribute<Telkoms, String> idpel;
    public static volatile SingularAttribute<Telkoms, String> saldo;
    public static volatile SingularAttribute<Telkoms, String> telref;
    public static volatile SingularAttribute<Telkoms, String> rescode;
    public static volatile SingularAttribute<Telkoms, String> restype;
    public static volatile SingularAttribute<Telkoms, String> nama;
    public static volatile SingularAttribute<Telkoms, String> produk;
    public static volatile SingularAttribute<Telkoms, String> response;
    public static volatile SingularAttribute<Telkoms, String> bulan;
    public static volatile SingularAttribute<Telkoms, String> user;
    public static volatile SingularAttribute<Telkoms, Long> inboxId;
    public static volatile SingularAttribute<Telkoms, String> jumlahrek;
    public static volatile SingularAttribute<Telkoms, String> status;

}