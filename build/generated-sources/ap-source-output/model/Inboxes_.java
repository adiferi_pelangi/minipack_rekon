package model;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2019-09-13T19:30:25")
@StaticMetamodel(Inboxes.class)
public class Inboxes_ { 

    public static volatile SingularAttribute<Inboxes, Date> statusDate;
    public static volatile SingularAttribute<Inboxes, String> receiver;
    public static volatile SingularAttribute<Inboxes, Boolean> isResponse;
    public static volatile SingularAttribute<Inboxes, String> trxType;
    public static volatile SingularAttribute<Inboxes, Integer> terminal;
    public static volatile SingularAttribute<Inboxes, Integer> prodId;
    public static volatile SingularAttribute<Inboxes, String> message;
    public static volatile SingularAttribute<Inboxes, Integer> userId;
    public static volatile SingularAttribute<Inboxes, String> trxId;
    public static volatile SingularAttribute<Inboxes, String> sender;
    public static volatile SingularAttribute<Inboxes, Integer> mediaTypeId;
    public static volatile SingularAttribute<Inboxes, String> senderType;
    public static volatile SingularAttribute<Inboxes, Boolean> freeOfCharge;
    public static volatile SingularAttribute<Inboxes, Integer> id;
    public static volatile SingularAttribute<Inboxes, Integer> status;
    public static volatile SingularAttribute<Inboxes, Date> createDate;

}