package model;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2019-09-13T19:30:25")
@StaticMetamodel(Mutations.class)
public class Mutations_ { 

    public static volatile SingularAttribute<Mutations, String> note;
    public static volatile SingularAttribute<Mutations, Long> amount;
    public static volatile SingularAttribute<Mutations, Date> updateDate;
    public static volatile SingularAttribute<Mutations, String> createBy;
    public static volatile SingularAttribute<Mutations, Long> balance;
    public static volatile SingularAttribute<Mutations, String> updateBy;
    public static volatile SingularAttribute<Mutations, Character> jenis;
    public static volatile SingularAttribute<Mutations, Long> id;
    public static volatile SingularAttribute<Mutations, Long> version;
    public static volatile SingularAttribute<Mutations, Integer> userId;
    public static volatile SingularAttribute<Mutations, Integer> inboxId;
    public static volatile SingularAttribute<Mutations, Date> createDate;

}