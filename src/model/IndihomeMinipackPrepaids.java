/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.io.Serializable;
import java.math.BigInteger;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author BIGDATAMEDIA
 */
@Entity
@Table(name = "indihome_minipack_prepaids")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "IndihomeMinipackPrepaids.findAll", query = "SELECT i FROM IndihomeMinipackPrepaids i")
    , @NamedQuery(name = "IndihomeMinipackPrepaids.findById", query = "SELECT i FROM IndihomeMinipackPrepaids i WHERE i.id = :id")
    , @NamedQuery(name = "IndihomeMinipackPrepaids.findByInboxId", query = "SELECT i FROM IndihomeMinipackPrepaids i WHERE i.inboxId = :inboxId")
    , @NamedQuery(name = "IndihomeMinipackPrepaids.findByProductType", query = "SELECT i FROM IndihomeMinipackPrepaids i WHERE i.productType = :productType")
    , @NamedQuery(name = "IndihomeMinipackPrepaids.findByProduk", query = "SELECT i FROM IndihomeMinipackPrepaids i WHERE i.produk = :produk")
    , @NamedQuery(name = "IndihomeMinipackPrepaids.findBySettlementDate", query = "SELECT i FROM IndihomeMinipackPrepaids i WHERE i.settlementDate = :settlementDate")
    , @NamedQuery(name = "IndihomeMinipackPrepaids.findByAmount", query = "SELECT i FROM IndihomeMinipackPrepaids i WHERE i.amount = :amount")
    , @NamedQuery(name = "IndihomeMinipackPrepaids.findByIdpel", query = "SELECT i FROM IndihomeMinipackPrepaids i WHERE i.idpel = :idpel")
    , @NamedQuery(name = "IndihomeMinipackPrepaids.findByCrmName", query = "SELECT i FROM IndihomeMinipackPrepaids i WHERE i.crmName = :crmName")
    , @NamedQuery(name = "IndihomeMinipackPrepaids.findByPreName", query = "SELECT i FROM IndihomeMinipackPrepaids i WHERE i.preName = :preName")
    , @NamedQuery(name = "IndihomeMinipackPrepaids.findByPrice", query = "SELECT i FROM IndihomeMinipackPrepaids i WHERE i.price = :price")})
public class IndihomeMinipackPrepaids implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Long id;
    @Column(name = "inbox_id")
    private BigInteger inboxId;
    @Size(max = 50)
    @Column(name = "product_type")
    private String productType;
    @Size(max = 50)
    @Column(name = "produk")
    private String produk;
    @Size(max = 50)
    @Column(name = "settlement_date")
    private String settlementDate;
    @Column(name = "amount")
    private Integer amount;
    @Size(max = 50)
    @Column(name = "idpel")
    private String idpel;
    @Size(max = 50)
    @Column(name = "crm_name")
    private String crmName;
    @Size(max = 50)
    @Column(name = "pre_name")
    private String preName;
    @Size(max = 50)
    @Column(name = "price")
    private String price;

    public IndihomeMinipackPrepaids() {
    }

    public IndihomeMinipackPrepaids(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public BigInteger getInboxId() {
        return inboxId;
    }

    public void setInboxId(BigInteger inboxId) {
        this.inboxId = inboxId;
    }

    public String getProductType() {
        return productType;
    }

    public void setProductType(String productType) {
        this.productType = productType;
    }

    public String getProduk() {
        return produk;
    }

    public void setProduk(String produk) {
        this.produk = produk;
    }

    public String getSettlementDate() {
        return settlementDate;
    }

    public void setSettlementDate(String settlementDate) {
        this.settlementDate = settlementDate;
    }

    public Integer getAmount() {
        return amount;
    }

    public void setAmount(Integer amount) {
        this.amount = amount;
    }

    public String getIdpel() {
        return idpel;
    }

    public void setIdpel(String idpel) {
        this.idpel = idpel;
    }

    public String getCrmName() {
        return crmName;
    }

    public void setCrmName(String crmName) {
        this.crmName = crmName;
    }

    public String getPreName() {
        return preName;
    }

    public void setPreName(String preName) {
        this.preName = preName;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof IndihomeMinipackPrepaids)) {
            return false;
        }
        IndihomeMinipackPrepaids other = (IndihomeMinipackPrepaids) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "model.IndihomeMinipackPrepaids[ id=" + id + " ]";
    }
    
}
