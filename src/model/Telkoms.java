/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author kurnia
 */
@Entity
@Table(name = "telkoms")
@NamedQueries({
    @NamedQuery(name = "Telkoms.findAll", query = "SELECT t FROM Telkoms t")
    , @NamedQuery(name = "Telkoms.findById", query = "SELECT t FROM Telkoms t WHERE t.id = :id")
    , @NamedQuery(name = "Telkoms.findByTransid", query = "SELECT t FROM Telkoms t WHERE t.transid = :transid")
    , @NamedQuery(name = "Telkoms.findByPartnertid", query = "SELECT t FROM Telkoms t WHERE t.partnertid = :partnertid")
    , @NamedQuery(name = "Telkoms.findByRestype", query = "SELECT t FROM Telkoms t WHERE t.restype = :restype")
    , @NamedQuery(name = "Telkoms.findByRescode", query = "SELECT t FROM Telkoms t WHERE t.rescode = :rescode")
    , @NamedQuery(name = "Telkoms.findByIdpel", query = "SELECT t FROM Telkoms t WHERE t.idpel = :idpel")
    , @NamedQuery(name = "Telkoms.findByNama", query = "SELECT t FROM Telkoms t WHERE t.nama = :nama")
    , @NamedQuery(name = "Telkoms.findByDatel", query = "SELECT t FROM Telkoms t WHERE t.datel = :datel")
    , @NamedQuery(name = "Telkoms.findByTagihan", query = "SELECT t FROM Telkoms t WHERE t.tagihan = :tagihan")
    , @NamedQuery(name = "Telkoms.findByBulan", query = "SELECT t FROM Telkoms t WHERE t.bulan = :bulan")
    , @NamedQuery(name = "Telkoms.findByJumlahrek", query = "SELECT t FROM Telkoms t WHERE t.jumlahrek = :jumlahrek")
    , @NamedQuery(name = "Telkoms.findByAdmin", query = "SELECT t FROM Telkoms t WHERE t.admin = :admin")
    , @NamedQuery(name = "Telkoms.findByNpwp", query = "SELECT t FROM Telkoms t WHERE t.npwp = :npwp")
    , @NamedQuery(name = "Telkoms.findByTelref", query = "SELECT t FROM Telkoms t WHERE t.telref = :telref")
    , @NamedQuery(name = "Telkoms.findByRefnbr", query = "SELECT t FROM Telkoms t WHERE t.refnbr = :refnbr")
    , @NamedQuery(name = "Telkoms.findBySaldo", query = "SELECT t FROM Telkoms t WHERE t.saldo = :saldo")
    , @NamedQuery(name = "Telkoms.findByXml", query = "SELECT t FROM Telkoms t WHERE t.xml = :xml")
    , @NamedQuery(name = "Telkoms.findByCreateDate", query = "SELECT t FROM Telkoms t WHERE t.createDate = :createDate")
    , @NamedQuery(name = "Telkoms.findByDestination", query = "SELECT t FROM Telkoms t WHERE t.destination = :destination")
    , @NamedQuery(name = "Telkoms.findByUser", query = "SELECT t FROM Telkoms t WHERE t.user = :user")
    , @NamedQuery(name = "Telkoms.findByPass", query = "SELECT t FROM Telkoms t WHERE t.pass = :pass")
    , @NamedQuery(name = "Telkoms.findByInboxId", query = "SELECT t FROM Telkoms t WHERE t.inboxId = :inboxId")
    , @NamedQuery(name = "Telkoms.findByReprint", query = "SELECT t FROM Telkoms t WHERE t.reprint = :reprint")
    , @NamedQuery(name = "Telkoms.findByProduk", query = "SELECT t FROM Telkoms t WHERE t.produk = :produk")
    , @NamedQuery(name = "Telkoms.findByStatus", query = "SELECT t FROM Telkoms t WHERE t.status = :status")
    , @NamedQuery(name = "Telkoms.findByAck", query = "SELECT t FROM Telkoms t WHERE t.ack = :ack")
    , @NamedQuery(name = "Telkoms.findByJmlAdmin", query = "SELECT t FROM Telkoms t WHERE t.jmlAdmin = :jmlAdmin")
    , @NamedQuery(name = "Telkoms.findByJmlBayar", query = "SELECT t FROM Telkoms t WHERE t.jmlBayar = :jmlBayar")
    , @NamedQuery(name = "Telkoms.findByResponse", query = "SELECT t FROM Telkoms t WHERE t.response = :response")})


@SequenceGenerator(sequenceName="telkoms_id_seq",name="telkom_gen")
public class Telkoms implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "telkom_gen")
    @Basic(optional = false)
    @Column(name = "id")
    private Long id;
    @Size(max = 50)
    @Column(name = "transid")
    private String transid;
    @Size(max = 50)
    @Column(name = "partnertid")
    private String partnertid;
    @Size(max = 50)
    @Column(name = "restype")
    private String restype;
    @Size(max = 50)
    @Column(name = "rescode")
    private String rescode;
    @Size(max = 50)
    @Column(name = "idpel")
    private String idpel;
    @Size(max = 255)
    @Column(name = "nama")
    private String nama;
    @Size(max = 255)
    @Column(name = "datel")
    private String datel;
    @Size(max = 50)
    @Column(name = "tagihan")
    private String tagihan;
    @Size(max = 50)
    @Column(name = "bulan")
    private String bulan;
    @Size(max = 50)
    @Column(name = "jumlahrek")
    private String jumlahrek;
    @Size(max = 50)
    @Column(name = "admin")
    private String admin;
    @Size(max = 50)
    @Column(name = "npwp")
    private String npwp;
    @Size(max = 50)
    @Column(name = "telref")
    private String telref;
    @Size(max = 50)
    @Column(name = "refnbr")
    private String refnbr;
    @Size(max = 50)
    @Column(name = "saldo")
    private String saldo;
    @Size(max = 2147483647)
    @Column(name = "xml")
    private String xml;
    @Column(name = "create_date")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createDate;
    @Size(max = 500)
    @Column(name = "destination")
    private String destination;
    @Size(max = 255)
    @Column(name = "user_")
    private String user;
    @Size(max = 255)
    @Column(name = "pass_")
    private String pass;
    @Column(name = "inbox_id")
    private Long inboxId;
    @Column(name = "reprint")
    private Short reprint;
    @Size(max = 255)
    @Column(name = "produk")
    private String produk;
    @Size(max = 255)
    @Column(name = "status")
    private String status;
    @Size(max = 255)
    @Column(name = "ack")
    private String ack;
    @Size(max = 255)
    @Column(name = "jml_admin")
    private String jmlAdmin;
    @Size(max = 255)
    @Column(name = "jml_bayar")
    private String jmlBayar;
    @Size(max = 2147483647)
    @Column(name = "response")
    private String response;

    public Telkoms() {
    }

    public Telkoms(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTransid() {
        return transid;
    }

    public void setTransid(String transid) {
        this.transid = transid;
    }

    public String getPartnertid() {
        return partnertid;
    }

    public void setPartnertid(String partnertid) {
        this.partnertid = partnertid;
    }

    public String getRestype() {
        return restype;
    }

    public void setRestype(String restype) {
        this.restype = restype;
    }

    public String getRescode() {
        return rescode;
    }

    public void setRescode(String rescode) {
        this.rescode = rescode;
    }

    public String getIdpel() {
        return idpel;
    }

    public void setIdpel(String idpel) {
        this.idpel = idpel;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public String getDatel() {
        return datel;
    }

    public void setDatel(String datel) {
        this.datel = datel;
    }

    public String getTagihan() {
        return tagihan;
    }

    public void setTagihan(String tagihan) {
        this.tagihan = tagihan;
    }

    public String getBulan() {
        return bulan;
    }

    public void setBulan(String bulan) {
        this.bulan = bulan;
    }

    public String getJumlahrek() {
        return jumlahrek;
    }

    public void setJumlahrek(String jumlahrek) {
        this.jumlahrek = jumlahrek;
    }

    public String getAdmin() {
        return admin;
    }

    public void setAdmin(String admin) {
        this.admin = admin;
    }

    public String getNpwp() {
        return npwp;
    }

    public void setNpwp(String npwp) {
        this.npwp = npwp;
    }

    public String getTelref() {
        return telref;
    }

    public void setTelref(String telref) {
        this.telref = telref;
    }

    public String getRefnbr() {
        return refnbr;
    }

    public void setRefnbr(String refnbr) {
        this.refnbr = refnbr;
    }

    public String getSaldo() {
        return saldo;
    }

    public void setSaldo(String saldo) {
        this.saldo = saldo;
    }

    public String getXml() {
        return xml;
    }

    public void setXml(String xml) {
        this.xml = xml;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public String getDestination() {
        return destination;
    }

    public void setDestination(String destination) {
        this.destination = destination;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public String getPass() {
        return pass;
    }

    public void setPass(String pass) {
        this.pass = pass;
    }

    public Long getInboxId() {
        return inboxId;
    }

    public void setInboxId(Long inboxId) {
        this.inboxId = inboxId;
    }

    public Short getReprint() {
        return reprint;
    }

    public void setReprint(Short reprint) {
        this.reprint = reprint;
    }

    public String getProduk() {
        return produk;
    }

    public void setProduk(String produk) {
        this.produk = produk;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getAck() {
        return ack;
    }

    public void setAck(String ack) {
        this.ack = ack;
    }

    public String getJmlAdmin() {
        return jmlAdmin;
    }

    public void setJmlAdmin(String jmlAdmin) {
        this.jmlAdmin = jmlAdmin;
    }

    public String getJmlBayar() {
        return jmlBayar;
    }

    public void setJmlBayar(String jmlBayar) {
        this.jmlBayar = jmlBayar;
    }

    public String getResponse() {
        return response;
    }

    public void setResponse(String response) {
        this.response = response;
    }

    
    
    @Override
    public int hashCode() {
        int hash = 0;
        hash += (getId() != null ? getId().hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Telkoms)) {
            return false;
        }
        Telkoms other = (Telkoms) object;
        if ((this.getId() == null && other.getId() != null) || (this.getId() != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }
    
    @Override
    public String toString() {
        return "model.Telkoms[ id=" + id + " ]";
    }
    
}
