/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fira;


import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.io.StringReader;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.ParserConfigurationException;
 
import org.apache.commons.net.PrintCommandListener;
import org.apache.commons.net.ftp.FTP;
import org.apache.commons.net.ftp.FTPClient;
import org.apache.commons.net.ftp.FTPFile;
import org.apache.commons.net.ftp.FTPReply;
import org.w3c.dom.CharacterData;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

public class aa {
     public static void main(String[] args) throws ParserConfigurationException, SAXException, IOException {
     String xmlRecords = "<?xml version='1.0' encoding='UTF-8'?>\n" +
"<response>\n" +
"	<product_type>INDIHOME MINIPACK PREPAID</product_type>\n" +
"	<mti>0210</mti>\n" +
"	<pan>084009</pan>\n" +
"	<processing_code>170000</processing_code>\n" +
"	<amount>000000072000</amount>\n" +
"	<transmission_date_time>0830104330</transmission_date_time>\n" +
"	<stan>000000327774</stan>\n" +
"	<local_trx_date>104330</local_trx_date>\n" +
"	<local_trx_time>0830</local_trx_time>\n" +
"	<merchant_type>6021</merchant_type>\n" +
"	<rc>0000</rc>\n" +
"	<settlement_date>0831</settlement_date>\n" +
"	<noInternet>121202221243   </noInternet>\n" +
"	<infoPackage>\n" +
"		<packageItem>\n" +
"			<crmname>PREUSEEKIDDYH</crmname>\n" +
"			<prename>INDIKIDS</prename>\n" +
"			<price>72000</price>\n" +
"		</packageItem>\n" +
"	</infoPackage>\n" +
"</response>";

    try {
        DocumentBuilderFactory dbf =
            DocumentBuilderFactory.newInstance();
        DocumentBuilder db = dbf.newDocumentBuilder();
        InputSource is = new InputSource();
        is.setCharacterStream(new StringReader(xmlRecords));
        
        Document doc = db.parse(is);
        NodeList nodes = doc.getElementsByTagName("infoPackage");
        
        // iterate the employees
            for (int i = 0; i < nodes.getLength(); i++) {
               Element element = (Element) nodes.item(i);

               NodeList name = element.getElementsByTagName("crmname");
               Element line = (Element) name.item(0);
               System.out.println("crmname: " + getCharacterDataFromElement(line));
            }
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        
     }
     
      public static String getCharacterDataFromElement(Element e) {
    Node child = e.getFirstChild();
    if (child instanceof CharacterData) {
       CharacterData cd = (CharacterData) child;
       return cd.getData();
    }
    return "?";
  }
 
}
