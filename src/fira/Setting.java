/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fira;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.*;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author ngonar
 */
public class Setting {

    private String dbUrl;
    private String dbDriver;
    private String dbUser;
    private String dbPass;
    private String dbUrlSuspect;
    private String dbDriverSuspect;
    private String dbUserSuspect;
    private String dbPassSuspect;
    private String cid;
    private String transactionFrom;
    private String ftpHost = "";
    private String ftpPort = "";
    private String ftpUsername = "";
    private String ftpPassword = "";
    private String mitra = "";
    private String partnerProduk = "";
    private String productCodeDesc = "";
    private String areaDistribusi = "";
    private String autoRevisi = "off";
    private String switcherId = "";
    private String halo = "";
    private String telkom = "";
    private String tvkpost = "";
    private String kode_ca = "";
    private String kode_sub_ca = "";
    private String tvkpre = "";

    public Setting() {
        try {
            getFileSetting();
            //System.out.println("==>" + dbUrl + ", " + dbUser + ", " + dbPass);

        } catch (FileNotFoundException ex) {
            System.out.println("==>1");
            Logger.getLogger(Setting.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            System.out.println("==>2");
            Logger.getLogger(Setting.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

//    public void setConnection(String dbDriver) {
//        this.dbDriver = dbDriver;
//    }
    public Connection getConnection() {
        Connection con = null;
        System.out.println(dbUrl + ", " + dbUser + ", " + dbPass);
        try {
            Class.forName(dbDriver);
            con = DriverManager.getConnection(dbUrl, dbUser, dbPass);

            System.out.println("Connection OTO ok.");

            return con;

        } catch (Exception e) {
            System.err.println("Exception OTO : " + e.getMessage());
        }

        return con;
    }

    public Connection getConnectionSuspect() {
        Connection con = null;
        System.out.println(dbUrlSuspect + ", " + dbUserSuspect + ", " + dbPassSuspect);
        try {
            Class.forName(dbDriverSuspect);
            con = DriverManager.getConnection(dbUrlSuspect, dbUserSuspect, dbPassSuspect);

            System.out.println("Connection OTO ok.");

            return con;

        } catch (Exception e) {
            System.err.println("Exception OTO : " + e.getMessage());
        }

        return con;
    }

    public String getCid() {
        return cid;
    }

    private void getFileSetting() throws FileNotFoundException, IOException {
        File f = new File("setting.properties");
        if (f.exists()) {
            Properties pro = new Properties();
            FileInputStream in = new FileInputStream(f);
            pro.load(in);

            dbDriver = pro.getProperty("Application.database.driver");
            //setConnection(dbDriver);
            dbUrl = pro.getProperty("Application.database.url");

            dbUser = pro.getProperty("Application.database.user");
            dbPass = pro.getProperty("Application.database.pass");

            dbDriverSuspect = pro.getProperty("Application.database.driverSuspect");
            //setConnection(dbDriver);
            dbUrlSuspect = pro.getProperty("Application.database.urlSuspect");
            dbUserSuspect = pro.getProperty("Application.database.userSuspect");
            dbPassSuspect = pro.getProperty("Application.database.passSuspect");

            ftpHost = pro.getProperty("Application.ftp.host");
            ftpUsername = pro.getProperty("Application.ftp.username");
            ftpPassword = pro.getProperty("Application.ftp.password");
            ftpPort = pro.getProperty("Application.ftp.port");

            cid = pro.getProperty("Switching.hulu.cid");

            mitra = pro.getProperty("Application.mitra");
            telkom = pro.getProperty("Application.produk.telkom");
            halo = pro.getProperty("Application.produk.halo");
            tvkpost = pro.getProperty("Application.produk.tvkpost");
            tvkpre = pro.getProperty("Application.produk.tvkpre");
            
            kode_ca = pro.getProperty("Application.kode.ca");
            kode_sub_ca = pro.getProperty("Application.kode.sub_ca");
            
            productCodeDesc = pro.getProperty("Application.produk.desc");
            areaDistribusi = pro.getProperty("Application.area");
            switcherId = pro.getProperty("Application.switching.id");
            setAutoRevisi(pro.getProperty("Application.modul.autorevisi"));
//            setAreaDistribusi(areaDistribusi);            
        } else {
            System.out.println("File setting not found");
        }
    }

    public String getTrxFrom() {
        return transactionFrom;
    }

    /**
     * @return the ftpHost
     */
    public String getFtpHost() {
        return ftpHost;
    }

    /**
     * @return the ftpPort
     */
    public String getFtpPort() {
        return ftpPort;
    }

    /**
     * @return the ftpUsername
     */
    public String getFtpUsername() {
        return ftpUsername;
    }

    /**
     * @return the ftpPassword
     */
    public String getFtpPassword() {
        return ftpPassword;
    }

    /**
     * @return the mitra
     */
    public String getMitra() {
        return mitra;
    }

    public String getKode_ca() {
        return kode_ca;
    }

    public void setKode_ca(String kode_ca) {
        this.kode_ca = kode_ca;
    }

    public String getKode_sub_ca() {
        return kode_sub_ca;
    }

    public void setKode_sub_ca(String kode_sub_ca) {
        this.kode_sub_ca = kode_sub_ca;
    }

    /**
     * @return the partnerProduk
     */
    public String getPartnerProduk(String partnerId) throws FileNotFoundException, IOException {
        File f = new File("setting.properties");
        if (f.exists()) {
            Properties pro = new Properties();
            FileInputStream in = new FileInputStream(f);
            pro.load(in);

            partnerProduk = pro.getProperty("Application.produk." + partnerId);
        } else {
            System.out.println("File setting not found");
        }

        return partnerProduk;
    }

    public String getTvkpost() {
        return tvkpost;
    }

    public void setTvkpost(String tvkpost) {
        this.tvkpost = tvkpost;
    }

    public String getTvkpre() {
        return tvkpre;
    }

    public void setTvkpre(String tvkpre) {
        this.tvkpre = tvkpre;
    }
    

    /**
     * @param partnerProduk the partnerProduk to set
     */
    public void setPartnerProduk(String partnerProduk) {
        this.partnerProduk = partnerProduk;
    }

    /**
     * @return the productCodeDesc
     */
    public String getProductCodeDesc() {
        return productCodeDesc;
    }

    /**
     * @param productCodeDesc the productCodeDesc to set
     */
    public void setProductCodeDesc(String productCodeDesc) {
        this.productCodeDesc = productCodeDesc;
    }

    /**
     * @return the areaDistribusi
     */
    public String getAreaDistribusi() {
        return areaDistribusi;
    }

    public void setHalo(String halo) {
        this.halo = halo;
    }

    public String getHalo() {
        return halo;
    }

    /**
     * @return the ftpHost
     */
    public void setTelkom(String telkom) {
        this.telkom = telkom;
    }

    public String getTelkom() {
        return telkom;
    }

    /**
     * @param areaDistribusi the areaDistribusi to set
     */
    public void setAreaDistribusi(String areaDistribusi) {
        this.areaDistribusi = areaDistribusi;
    }

    /**
     * @return the autoRevisi
     */
    public String getAutoRevisi() {
        return autoRevisi;
    }

    /**
     * @param autoRevisi the autoRevisi to set
     */
    public void setAutoRevisi(String autoRevisi) {
        this.autoRevisi = autoRevisi;
    }

    /**
     * @return the switcherId
     */
    public String getSwitcherId() {
        return switcherId;
    }

    /**
     * @param switcherId the switcherId to set
     */
    public void setSwitcherId(String switcherId) {
        this.switcherId = switcherId;
    }
    
    
}
