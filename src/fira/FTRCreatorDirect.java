package fira;

import static fira.aa.getCharacterDataFromElement;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.net.SocketException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Calendar;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathFactory;
import org.xml.sax.InputSource;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.DocumentBuilder;

import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;
import java.io.StringReader;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Date;
import java.util.HashMap;
import org.xml.sax.InputSource;
import org.apache.commons.net.PrintCommandListener;
import org.apache.commons.net.ftp.FTP;
import org.apache.commons.net.ftp.FTPClient;
import org.apache.commons.net.ftp.FTPReply;
import static org.apache.openjpa.persistence.jest.JESTCommand.Format.xml;
import org.jpos.iso.packager.GenericPackager;
import org.w3c.dom.CharacterData;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

/**
 *
 * @author kurnia
 */
class Halo {

    String trx = "";
    String idpel = "";
    String ca = "";
    String prod_id = "";
    String divre = "";
    String jml = "";
    String amount = "";
    String noref = "";

    public void setTrx(String trx) {
        this.trx = trx;
    }

    /**
     * @return the productCodeDesc
     */
    public String getTrx() {
        return trx;
    }

    public void setIdpel(String idpel) {
        this.idpel = idpel;
    }

    /**
     * @return the productCodeDesc
     */
    public String getIdpel() {
        return idpel;
    }

    public void setCA(String ca) {
        this.ca = ca;
    }

    /**
     * @return the productCodeDesc
     */
    public String getCA() {
        return ca;
    }

    public void setProdid(String prod_id) {
        this.prod_id = prod_id;
    }

    /**
     * @return the productCodeDesc
     */
    public String getProdid() {
        return prod_id;
    }

    public void setDivre(String divre) {
        this.divre = divre;
    }

    /**
     * @return the productCodeDesc
     */
    public String getDivre() {
        return divre;
    }

    public void setJml(String divre) {
        this.jml = jml;
    }

    /**
     * @return the productCodeDesc
     */
    public String getJml() {
        return jml;
    }

    public void setAmount(String divre) {
        this.amount = amount;
    }

    /**
     * @return the productCodeDesc
     */
    public String getAmount() {
        return amount;
    }

    public void setNoref(String divre) {
        this.noref = noref;
    }

    /**
     * @return the productCodeDesc
     */
    public String getNoref() {
        return noref;
    }

}

public class FTRCreatorDirect {

    private static final org.apache.log4j.Logger logger = org.apache.log4j.Logger.getLogger("FTRCreatorDirect");
    public static int option = 1;
    public static String message = "";
    public static Setting setting = new Setting();
    public static Calendar cal = Calendar.getInstance();
    public static DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
    public static Calendar calFl = Calendar.getInstance();
    public static DateFormat dateFormatFl = new SimpleDateFormat("yyyyMMdd");
    public static String startParam = "";
    public static String endParam = "";
    public static String startParam2 = "";
    public static String endParam2 = "";
    public static String createParam = "";
    public static String fileDate = "";
    public static String partnerId = "";
    public static String date0 =null;
    public static String date1 =null;
    public static String tulis ="";
    public static NumberFormat formatter2 = new DecimalFormat("##,##0");

    public static void main(String[] args) throws XPathExpressionException {
        String banyakHari = "";
        
        Date dt = new Date();
        LocalDateTime.from(dt.toInstant().atZone(ZoneId.of("UTC"))).minusDays(1);
        String datakemaren = LocalDateTime.from(dt.toInstant().atZone(ZoneId.of("UTC"))).minusDays(1).toString();
//        System.out.println("data kemaren"+datakemaren.substring(0,10));
        String TanggalKemarin = datakemaren.substring(0,10);
        
        try {
        banyakHari = args[0];
        date0 = args[0];
        date1 = args[1];
            System.out.println("hay 1 "+date0);
            System.out.println("hay 2 "+date1);

        } catch (Exception e) {
            banyakHari = "5";
        }

        if ((date0!=null)) {
            startParam = date0;
            startParam2 = date0;
            endParam = date1;
            endParam2 = date1;
        }else{
            startParam = TanggalKemarin;
            startParam2 = TanggalKemarin;
            endParam = TanggalKemarin;
            endParam2 = TanggalKemarin;
        }
        System.out.println("Tanggal "+startParam);
        System.out.println("Tangga2 "+endParam);
        
//        System.out.println("asdas"+args[0]);
        String settlementStartTime = "[startTime]";
        String settlementEndTime = "[endTime]";

        Calendar calNow = Calendar.getInstance();
        DateFormat dateFormat1 = new SimpleDateFormat("yyyyMMdd");

        Calendar cal3 = Calendar.getInstance();
        cal3.add(Calendar.DATE, ((-1)));

        Calendar calNow2 = Calendar.getInstance();
        DateFormat dateFormat2 = new SimpleDateFormat("yyyy-MM-dd");

        Calendar cal = Calendar.getInstance();
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        DateFormat dateFormatSlash = new SimpleDateFormat("dd/MM/yyyy");
        DateFormat dateFormatSlashTime = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
//        cal.add(Calendar.DATE, (Integer.parseInt(banyakHari) * (-1)));

        

        
//        startParam = dateFormat.format(TanggalKemarin);
//        startParam2 = dateFormat.format(TanggalKemarin);
//        endParam = dateFormat2.format(TanggalKemarin);
//        endParam2 = dateFormatSlash.format(TanggalKemarin);

//        startParam = TanggalKemarin;
//        startParam2 = TanggalKemarin;
//        endParam = TanggalKemarin;
//        endParam2 = TanggalKemarin;

        createParam = dateFormatSlashTime.format(calNow2.getTime());

        fileDate = dateFormat1.format(calNow.getTime());

//        System.out.println("Start date was " + startParam);
//        System.out.println("end date was " + endParam);
        logger.info("filename is " + fileDate);
        logger.info("folder name is " + fileDate.substring(0, 6));

        String produkDesc = setting.getProductCodeDesc();

//        if (banyakHari.equalsIgnoreCase("0")) {
//            startParam = settlementStartTime.replace("[startTime]", startParam + " 00:00:00");
//            endParam = settlementEndTime.replace("[endTime]", endParam + " 12:00:00");
//        } else {
//            startParam = settlementStartTime.replace("[startTime]", startParam + " 00:00:00");
//            endParam = settlementEndTime.replace("[endTime]", endParam + " 00:00:00");
//        }
        
        System.err.println("start : "+startParam);
        System.err.println("end : "+endParam);

//        String partnerCid = setting.getMitra();//SH_plg12110004
        String partnerCid = "SH_VIRA";

//        String partnerCid = "REKON_VIRA";
        String switcherId = setting.getSwitcherId();
        String areaDistribusi = setting.getAreaDistribusi();
        String[] partnerCidX = partnerCid.split(",");

        for (int i = 0; i < partnerCidX.length; i++) {

            String[] partnerX = partnerCidX[i].split("_");
            String partnerNick = partnerX[0];
            partnerId = partnerX[1];
            System.out.println("partnerid : " + partnerX[1]);
            System.err.println(partnerId);
            try {
                String partnerProduk = "TVKMINIPRE:1000";

                String[] partnerProdukX = partnerProduk.split("\\,");

                for (int j = 0; j < partnerProdukX.length; j++) {
                    createFtr(partnerNick, partnerId, partnerProdukX[j], produkDesc);
                }

            } catch (Exception e) {
                Logger.getLogger(FTRCreatorDirect.class.getName()).log(Level.SEVERE, null, e);
            }
        }

    }

    private static void createFtr(String partnerNick, String partnerId, String kodeProduk_priceTemplate, String produkDesc) {
        //String hasil = "";
        long row = 0;
        //long row = 0;
        long grandAmount = 0;
        int grandLembar = 0;
        double grandAdminCharge = 0;
        double grandVat = 0;
        double grandLightingTax = 0;
        double grandInstallment = 0;
        double grandPowerPurchase = 0;
        SLSLogger slsLog = new SLSLogger();
        ParseISOMessage parse = new ParseISOMessage();
        String switchingCid = setting.getCid();
        System.out.println("SWiii" + switchingCid);
        String lastId = "0";
        String param = "";

        String[] kode_priceX = kodeProduk_priceTemplate.split(":");
        String kodeProduk = kode_priceX[0];
        System.out.println("==============================");

        int priceTemplate = Integer.parseInt(kode_priceX[1]);

        int limit = 10;
        int offset = 0;
        String limitSql = "";
        String sql = "";
        String vcode = "";
        String cidSql = "";
        int counter = limit;
        boolean istart = true;

        String kode = "";
        String ca = "";
        String sub_ca = "";
        try {

            Setting set = new Setting();
            Connection con = set.getConnection();

            Statement st = con.createStatement();
            int total_el_bill = 0, total_penalty = 0, total_insentif = 0, total_vat = 0;

            counter = 0;
            offset += limit;
            int jml_bil = 0;
            switchingCid = partnerId;
            String prod = "";
            System.out.println("---------------------------" + switchingCid);
            if ("TVKMINIPRE".equalsIgnoreCase(kodeProduk)) {
                prod = "Minipack";
                kode = setting.getTvkpost();
                ca = setting.getKode_ca();
                sub_ca = setting.getKode_sub_ca();
                String fields = "p.vcode as code,\n" +
                                "i.prod_id as inbox_prod_id,\n" +
                                "t.create_date as trans_create_date,\n" +
                                "abs(m.amount) as price_sell,o.id as admin,\n" +
                                "I.id as noref,\n" +
                                "o.id as resp,\n" +
                                ""+
                                "o.message as outbox_message,\n" +
                                "i.create_date as inbox_create_date,\n" +
                                "i.id as inbox_id,\n" +
                                "i.idpel as inbox_idpel,\n" +
                                "u.first_name as firstname,\n" +
                                "t.price_buy as price_buy, \n"+
                                "tel.pre_name as crname";

                sql = "   select " + fields + " from transactions as t "
                        + "join inboxes as i on t.inbox_id = i.id "
                        + "join mutations as m on i.id = m.inbox_id \n" +
                        "join outboxes as o on i.id = o.inbox_id \n" +
                        "join products as p on p.id = i.prod_id \n" +
                        "join users as u on u.id = i.user_id \n" +
                        "join indihome_minipack_prepaids as tel on i.id=tel.inbox_id"+
                        " where ";
                
                sql += ""                    
                    + "(t.create_date) >= '"+startParam+"'"
                    + "and (t.create_date) < '"+endParam+"' "
                    + "and p.vcode like '%"+kodeProduk+"%'";
    
                logger.info(sql);
                System.out.println("KODE SETING : " + setting.getTvkpost());
                System.out.println("KODE CA : " + setting.getKode_ca());
                System.out.println("KODE SUB_CA : " + setting.getKode_sub_ca());
                System.out.println(sql);
            }

            ResultSet rs = st.executeQuery(sql);
            System.out.println(rs);
            String tanggalTrx = "";
            String tanggalTrxTemp = "";

            String[] produkDescX = produkDesc.split(kodeProduk + "::");
            String[] productNameX = produkDescX[1].split(",");
            String productName = productNameX[0];
            System.out.println("Produk Name : " + productName);
            System.err.println("Kode Produk : " + kodeProduk);

            String inbox_prod_id = "";
            String amount = "";
            String inbox_idpel = "";
            String noref1 = "";
            String noref = "";
            String admin = "";
            String resp = "";
            String Infonama = "";
            String crnames = "";
            String bit48 = "";
            String outboxmessageXml = "";
            String infoNama = "";
            String[] divre = new String[100];

            while (rs.next()) {
                counter++;
                noref = rs.getString("noref");
                lastId = rs.getString("inbox_id");
                inbox_prod_id = rs.getString("inbox_prod_id");
                inbox_idpel = rs.getString("inbox_idpel");
                amount = rs.getString("price_sell");
                admin = rs.getString("admin");
                vcode = rs.getString("code");
                resp = rs.getString("resp");
                crnames = rs.getString("crname");
                outboxmessageXml = rs.getString("outbox_message");
                String outbox_message = rs.getString("outbox_message");
                tanggalTrxTemp = rs.getString("inbox_create_date").replace(":", "");
                tanggalTrxTemp = tanggalTrxTemp.replace(" ", "");
                tanggalTrxTemp = tanggalTrxTemp.replace("-", "");

                try {
                    String[] tx = tanggalTrxTemp.split("\\.");
                    tanggalTrx = tx[0];
                } catch (Exception e) {
                    logger.info("end with second");
                }

                int payment_status = 1;
                String payment_statusX = "";

                DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();

                InputSource source = new InputSource(new StringReader(outbox_message));
                XPath xpath = XPathFactory.newInstance()
                        .newXPath();
                Object response = xpath.evaluate("/response", source, XPathConstants.NODE);
                noref1 = xpath.evaluate("noreference", response);
                if (outbox_message.contains("payment_status")) {
                    String[] payStatTemp1 = outbox_message.split("<payment_status>");
                    String[] payStatTemp2 = payStatTemp1[1].split("</payment_status>");
                    payment_status = Integer.parseInt(payStatTemp2[0]);
                    System.out.println("payment status = " + payment_status);
                }
                payment_statusX = String.format("%3s", payment_status).replace(' ', '0');

                String angka = " ";
                try {
                    angka = String.format("%17s", Integer.parseInt(rs.getString("price_sell")) - (priceTemplate * payment_status)).replace(' ', '0');
                } catch (Exception e) {
                    e.printStackTrace();
                    try {
                        String[] payStatTemp1 = outbox_message.split("<amount>");
                        String[] payStatTemp2 = payStatTemp1[1].split("</amount>");
                        angka = String.format("%17s", Integer.parseInt(payStatTemp2[0]) - (priceTemplate * payment_status)).replace(' ', '0');
                        System.out.println("angka = " + angka);
                    } catch (Exception e2) {
                        angka = String.format("%17s", angka).replace(' ', '0');
                        e.printStackTrace();
                    }
                }

//=======================================================
//    PARSING XML DATA
    try {
        DocumentBuilderFactory dbf =
            DocumentBuilderFactory.newInstance();
        DocumentBuilder db = dbf.newDocumentBuilder();
        InputSource is = new InputSource();
        
        is.setCharacterStream(new StringReader(outboxmessageXml));
        Document doc = db.parse(is);
        NodeList nodes = doc.getElementsByTagName("infoPackage");
        
        // iterate the employees
            for (int i = 0; i < nodes.getLength(); i++) {
               Element element = (Element) nodes.item(i);
               NodeList name = element.getElementsByTagName("crmname");
               Element line = (Element) name.item(0);
                String crname = "";
                //chek 
                if( getCharacterDataFromElement(line) != null){
                crname = getCharacterDataFromElement(line);
                }else{
                crname = "";
                }
        
                
               System.out.println("crmname: " + crname);
               infoNama = crname;
            }
        }
        catch (Exception e) {
            e.printStackTrace();
        }
//============================================================

//                System.out.println("Hasil::1:" + lastId + ",2:" + inbox_idpel + ",3:6021,4:" + inbox_prod_id + ",5: DIVRE,6: 0,7:" + amount + ",8:" + noref1 + ",9:Response" + resp);

//                System.out.println("OUBOXES::" + outbox_message);
                String divres = "0";

                String Dates = tanggalTrx.substring(0, 8);
                String Times = tanggalTrx.substring(8, 14);

//                      String header = String.format("%-8s", "DATE TRX") + "|"                  
//                    + String.format("%-6s", "TIME TRX") + "|" 
//                    + String.format("%-13s", "CustomerID") + "|" 
//                    + String.format("%-6s", "KODE CA") + "|" 
//                    + String.format("%-6s", "KODE SUBCA") + "|" 
//                    + String.format("%-16s", " KODE PRODUK") + "|" 
//                    + String.format("%-10s", "AMOUNT") + "|" 
//                    + String.format("%-32s", "NOREFF") + "|"
//                    + String.format("%-32s", "ADD INFO / NAMA") + "|"


                tulis = Dates.trim() + "; " // TRX DATE
                        + Times.trim() + "; " //TRX TIME                        
                        + String.format("%13s", inbox_idpel).trim() + "; " //IDPEL
                        + String.format("%6s", ca).trim() + "; " //KODE CA
                        + String.format("%6s", sub_ca).trim() + "; " //KODE SUB CA
                        + String.format("%16s", kode).trim() + "; " //ID PRODUK
                        + String.format("%10s", amount).replace(' ', '0').trim() + "; " //AMOUNT
                        + String.format("%32s", noref).trim()+ "; " //noref
                        + String.format("%32s", crnames).trim() //noref
                        + "\n";
                System.err.println(tulis);
                System.out.println("TULIS::" + tulis);
                System.out.println("PRODUK:::" + productName);
//                slsLog.printToFile(tulis, partnerNick, productName + "-" + switchingCid, fileDate);
//                if ("SPEEDY".equalsIgnoreCase(productName)) {
//                    slsLog.printToFile(tulis, partnerNick, productName, fileDate,ca);
//                } else {
                    slsLog.printToFile(tulis, partnerNick, productName, fileDate,ca);
//                }

                row += 1;
                //kali 100 karena ada desimal 2 angka
                grandAmount += Long.parseLong(angka);
                grandLembar += payment_status;

                if (grandAmount < 0 || Long.parseLong(angka) < 0) {
                    System.out.println("===" + grandAmount + "=======" + angka + "=====");
                    System.exit(1);

                }

            }
            //print
            if(tulis==""){
             slsLog.printToFile(tulis, partnerNick, productName, fileDate,ca);
            }

            System.out.println("SELESAI");

            st.close();
            con.close();
        } catch (Exception e) {
            logger.info(e.toString());
            logger.info(e.getMessage());
            System.out.println(e.getMessage());
            logger.info("misi pengambilan data prepaid gagal");
        }

    }

    public static String[] parseBit48PrepaidPurchase(String msg) {
        String[] hasil = new String[65];
        logger.info("String to parse : " + msg);

        if (msg.length() == 241) {
            int[] seq = {7, 11, 12, 1, 32, 32, 8, 25, 4, 9, 1, 1, 10, 1, 10, 1, 10, 1, 10, 1, 10, 1, 12, 1, 10, 20};
            String[] title = {
                "switcher id", //0
                "meter serial number", //1
                "subscriber id", //2
                "flag", //3
                "pln ref no", //4
                "switcher ref no", //5
                "vending receive no", //6
                "Subscriber name", //7
                "subscriber segmentation", //8
                "Power consuming cat", //9
                "buying option", //10
                "minor unit admin charge", //11
                "admin charge", //12
                "minor stamp of duty", //13
                "stamp of duty", //14
                "minor unit vat", //15
                "vat", //16
                "minor unit lampu jalan ", //17
                "lampu jalan", //18
                "minor unit angsuran", //19
                "angsuran", //20
                "minor unit power purchase", //21
                "power purchase", //22
                "minor unit power purchase kwh unit", //23
                "purchase kwh unit", //24
                "token number" //25
            };
            try {
                int n = 0;
                int f = 0;
                int l = 0;
                for (int i = 0; i < seq.length; i++) {
                    hasil[i] = "";

                    if (seq[i] == 1) {
                        hasil[i] = String.valueOf(msg.charAt(l));
                        logger.info(String.format("%-50s", title[i]) + " : " + String.format("%-30s", hasil[i]) + " ==> " + hasil[i].length());
                        l += seq[i];
                    } else if (seq[i] > 1) {
                        f = n;
                        l = n + seq[i];
                        hasil[i] = msg.substring(f, l);
                        logger.info("f, l : " + f + "," + l);
                        logger.info(String.format("%-50s", title[i]) + " : " + String.format("%-30s", hasil[i]) + " ==> " + hasil[i].length());
                    }
                    n = l;
                }

            } catch (Exception e) {
                //logger.log(Level.FATAL, e.getMessage());
                e.printStackTrace();

            }

        } else {

            int[] seq = {7, 11, 12, 1, 32, 32, 8, 25, 4};
            String[] title = {
                "switcher id", //0
                "meter serial number", //1
                "subscriber id", //2
                "flag", //3
                "pln ref no", //4
                "switcher ref no", //5
                "vending receive no", //6
                "Subscriber name", //7
                "subscriber segmentation" //8
            };

            try {
                int n = 0;
                int f = 0;
                int l = 0;
                for (int i = 0; i < seq.length; i++) {
                    hasil[i] = "";

                    if (seq[i] == 1) {
                        hasil[i] = String.valueOf(msg.charAt(l));
                        logger.info(String.format("%-50s", title[i]) + " : " + String.format("%-30s", hasil[i]) + " ==> " + hasil[i].length());
                        l += seq[i];
                    } else if (seq[i] > 1) {
                        f = n;
                        l = n + seq[i];
                        hasil[i] = msg.substring(f, l);
                        logger.info("f, l : " + f + "," + l);
                        logger.info(String.format("%-50s", title[i]) + " : " + String.format("%-30s", hasil[i]) + " ==> " + hasil[i].length());
                    }
                    n = l;
                }

            } catch (Exception e) {
                //logger.log(Level.FATAL, e.getMessage());
                e.printStackTrace();

            }

        }

        return hasil;
    }

    public static String[] parseBit48PostpaidPayment(String msg) {
        String[] hasil = new String[65];

        try {

            for (int i = 0; i <= 64; i++) {
                hasil[i] = "";
            }

            hasil[0] = msg.substring(0, 7);
            logger.info("switcher id : " + hasil[0]);// switcher id - 7
            hasil[1] = msg.substring(7, 19);
            logger.info("sub id : " + hasil[1]);// subscriber id - 12
            hasil[2] = String.valueOf(msg.charAt(19));
            logger.info("bill status : " + hasil[2]); //bill status - 1
            hasil[3] = String.valueOf(msg.charAt(20));
            logger.info("payment status : " + hasil[3]); //bill status - 1
            hasil[4] = msg.substring(21, 23);
            logger.info("outstanding bill : " + hasil[4]); //outstanding bill - 2
            hasil[5] = msg.substring(23, 55);
            logger.info("switcher ref : " + hasil[5]); //switcher ref no - 32
            hasil[6] = msg.substring(55, 80);
            logger.info("sub name : " + hasil[6]); //Subscriber name - 25
            hasil[7] = msg.substring(80, 85);
            logger.info("service unit : " + hasil[7]); //Service unit - 5
            hasil[8] = msg.substring(85, 100);
            logger.info("service unit phone : " + hasil[8]); //Service unit phone - 15
            hasil[9] = msg.substring(100, 104);
            logger.info("sub segmentation : " + hasil[9]); //subscriber segmentation - 4
            hasil[10] = msg.substring(104, 113);
            logger.info("power consuming cat : " + hasil[10]);  //Power consuming cat - 9
            hasil[11] = msg.substring(113, 122);
            logger.info("total admin charge : " + hasil[11]); //Total admin charges - 9

            hasil[12] = msg.substring(122, 128);
            logger.info("bill period : " + hasil[12]); //bill period - 6
            hasil[13] = msg.substring(128, 136);
            logger.info("due date : " + hasil[13]); //due date - 8
            hasil[14] = msg.substring(136, 144);
            logger.info("meter read date : " + hasil[14]); //meter read date - 8
            hasil[15] = msg.substring(144, 155);
            logger.info("total electricity bill : " + hasil[15]); //total electricity bill - 11
            hasil[16] = msg.substring(155, 166);
            logger.info("incentive : " + hasil[16]); //incentive - 11
            hasil[17] = msg.substring(166, 176);
            logger.info("vat : " + hasil[17]); //vat - 10
            hasil[18] = msg.substring(176, 185);
            logger.info("penalty fee : " + hasil[18]); //penalty fee - 9
            hasil[19] = msg.substring(185, 193);
            logger.info("prev meter reading : " + hasil[19]); //prev meter reading 1 - 8
            hasil[20] = msg.substring(193, 201);
            logger.info("curr meter reading : " + hasil[20]); //curr meter reading 1 - 8
            hasil[21] = msg.substring(201, 209);
            logger.info("prev meter reading 2 : " + hasil[21]); //prev meter reading 2 - 8
            hasil[22] = msg.substring(209, 217);
            logger.info("curr meter reading 2 : " + hasil[22]); //curr meter reading 2 - 8
            hasil[23] = msg.substring(217, 225);
            logger.info("prev meter reading 3 : " + hasil[23]); //prev meter reading 3 - 8
            hasil[24] = msg.substring(225, 233);
            logger.info("curr meter reading 3 : " + hasil[24]); //curr meter reading 3 - 8

            if (Integer.parseInt(hasil[2]) > 1) {
                hasil[24 + 1] = msg.substring(233, 239); //bill period - 6
                hasil[25 + 1] = msg.substring(239, 247); //due date - 8
                hasil[26 + 1] = msg.substring(247, 255); //meter read date - 8
                hasil[27 + 1] = msg.substring(255, 266); //total electricity bill - 11
                hasil[28 + 1] = msg.substring(266, 277); //incentive - 11
                hasil[29 + 1] = msg.substring(277, 287); //vat - 10
                hasil[30 + 1] = msg.substring(287, 296); //penalty fee - 9
                hasil[31 + 1] = msg.substring(296, 304); //prev meter reading 1 - 8
                hasil[32 + 1] = msg.substring(304, 312); //curr meter reading 1 - 8
                hasil[33 + 1] = msg.substring(312, 320); //prev meter reading 2 - 8
                hasil[34 + 1] = msg.substring(320, 328); //prev meter reading 2 - 8
                hasil[35 + 1] = msg.substring(328, 336); //prev meter reading 3 - 8
                hasil[36 + 1] = msg.substring(336, 344); //prev meter reading 3 - 8
            }

            if (Integer.parseInt(hasil[2]) > 2) {
                hasil[37 + 1] = msg.substring(344, 350);
                hasil[38 + 1] = msg.substring(350, 358);
                hasil[39 + 1] = msg.substring(358, 366);
                hasil[40 + 1] = msg.substring(366, 377);
                hasil[41 + 1] = msg.substring(377, 388);
                hasil[42 + 1] = msg.substring(388, 398);
                hasil[43 + 1] = msg.substring(398, 407);
                hasil[44 + 1] = msg.substring(407, 415);
                hasil[45 + 1] = msg.substring(415, 423);
                hasil[46 + 1] = msg.substring(423, 431);
                hasil[47 + 1] = msg.substring(431, 439);
                hasil[48 + 1] = msg.substring(439, 447);
                hasil[49 + 1] = msg.substring(447, 455);
            }

            if (Integer.parseInt(hasil[2]) > 3) {
                hasil[50 + 1] = msg.substring(455, 461);
                hasil[51 + 1] = msg.substring(461, 469);
                hasil[52 + 1] = msg.substring(469, 477);
                hasil[53 + 1] = msg.substring(477, 488);
                hasil[54 + 1] = msg.substring(488, 499);
                hasil[55 + 1] = msg.substring(499, 509);
                hasil[56 + 1] = msg.substring(509, 518);
                hasil[57 + 1] = msg.substring(518, 526);
                hasil[58 + 1] = msg.substring(526, 534);
                hasil[59 + 1] = msg.substring(534, 542);
                hasil[60 + 1] = msg.substring(542, 550);
                hasil[61 + 1] = msg.substring(550, 558);
                hasil[62 + 1] = msg.substring(558, 565);
            }

        } catch (Exception e) {
            e.printStackTrace();
            //logger.log(Level.FATAL, e.getMessage());
        }

        return hasil;
    }

    static double grandTotalAmountNon = 0;
    static double grandTransAmountNon = 0;
    static double grandAdminChargesNon = 0;
    static long rowNon = 0;

    private static void createRecap(Map<String, String> response, boolean isLogged, boolean isRepeat) {
        SLSLogger slsLog = new SLSLogger();

        //if (isLogged) {
        //log to file
        try {
            String[] bit48 = parseBit48NonPayment(response.get("48"));

            //bila menggunakan payment
            double biayaAdmin = 0;
            try {
                biayaAdmin = Double.parseDouble(bit48[18]);
            } catch (Exception e) {
                biayaAdmin = 0;
                logger.info("Biaya admin == 0");
            }

            logger.info("biaya admin " + biayaAdmin + " amount : " + Double.parseDouble(bit48[14]));
            double totalAmount = Double.parseDouble(bit48[14]) + biayaAdmin;
            String toLog = String.format("%14s", response.get("12")).replace(' ', '0') + "|" //DT
                    + String.format("%7s", response.get("33")).replace(' ', '0') + "|" //CENTRAL_ID
                    + String.format("%4s", response.get("26")).replace(' ', '0') + "|" //MERCHANT
                    + String.format("%32s", bit48[8]).replace(' ', '0') + "|" //REFFNUM
                    + String.format("%32s", bit48[9]).replace(' ', '0') + "|" //SREFFNUM
                    + String.format("%12s", bit48[6]).replace(' ', '0') + "|" //SUB_ID
                    + String.format("%13s", bit48[1]).replace(' ', '0') + "|" //REG_NUM
                    + String.format("%8s", bit48[4]).replace(' ', '0') + "|" //REG_DATE
                    + String.format("%3s", bit48[2]).replace(' ', '0') + "|" //TRX_CODE
                    + String.format("%17s", bit48[14].substring(0, (bit48[14].length() - 2))).replace(' ', '0') + "|" //TOTAL_AMOUNT
                    + String.format("%17s", bit48[16].substring(0, (bit48[16].length() - 2))).replace(' ', '0') + "|" //TRANS_AMOUNT
                    + String.format("%10s", bit48[18].substring(0, (bit48[18].length() - 2))).replace(' ', '0') + "|" //ADMIN_CHARGES
                    + String.format("%7s", response.get("32")).replace(' ', '0') + "|" //BANK_CODE
                    + String.format("%16s", response.get("41")).replace(' ', '0')//PPID
                    + "\n";
//                    ;

            slsLog.recapPLNNon(toLog, setting.getCid() + "-50504-99", fileDate);

            rowNon += 1;
            //grandTotalAmountNon += totalAmount;
            //grandTransAmountNon += Double.parseDouble(bit48[14]);

            grandTotalAmountNon += Double.parseDouble(bit48[14].substring(0, (bit48[14].length() - 2)));
            grandTransAmountNon += Double.parseDouble(bit48[16].substring(0, (bit48[16].length() - 2)));

            grandAdminChargesNon += Double.parseDouble(bit48[18].substring(0, (bit48[18].length() - 2)));

        } catch (Exception e) {
            e.printStackTrace();
        }
        //}
    }
    
    //PARISNG XML
    public static String getCharacterDataFromElement(Element e) {
        if(e!=null){
        Node child = e.getFirstChild();
                if (child instanceof CharacterData) {
                    CharacterData cd = (CharacterData) child;
                    return cd.getData();
                }
        }
        return "tidak tersedia";
    }

    private static String[] parseBit48NonPayment(String msg) {
        //logConsoleTitle("PASE BIT48 PAYMENT");
        String[] hasil = new String[65];

        int[] seq = {7, 13, 3, 25, 8, 8, 12, 25, 32, 32, 5, 35, 15, 1, 17, 1, 17, 1, 10};
        String[] title = {
            "switcher id",
            "reg no.",
            "trx code",
            "trx name",
            "reg date",
            "exp date",
            "subsc id",
            "subsc name",
            "PLN Reference Number",
            "SLS Receipt reference",
            "Service Unit",
            "Service Unit Address",
            "Service Unit Phone",
            "Total transaction amount minor unit",
            "Total transaction amount",
            "PLN bill minor unit",
            "PLN bill",
            "Adm Charge minor unit",
            "Adm Charge"
        };

        try {
            int n = 0;
            int f = 0;
            int l = 0;
            for (int i = 0; i < seq.length; i++) {
                hasil[i] = "";

                if (seq[i] == 1) {
                    hasil[i] = String.valueOf(msg.charAt(l));
                    logger.info(String.format("%-50s", title[i]) + " : " + String.format("%-30s", hasil[i]) + " ==> " + hasil[i].length());
                    l += seq[i];
                } else if (seq[i] > 1) {
                    f = n;
                    l = n + seq[i];
                    System.out.print(String.format("%-50s", title[i]) + " : ");
                    hasil[i] = msg.substring(f, l);
                    //logger.info(String.format("%-50s", title[i]) + " : " + String.format("%-30s", hasil[i]) + " ==> " + hasil[i].length());
                    logger.info(String.format("%-30s", hasil[i]) + " ==> " + hasil[i].length());
                }
                n = l;
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return hasil;
    }

    private static void upload() {
        String switchingCid = setting.getCid();

        String[] type = {"Postpaid", "Prepaid", "Nontaglis"};
        String[] kode = {switchingCid + "-50501-99", switchingCid + "-50502-99", switchingCid + "-50504-99"};

        //note: cari barang yang baru dibuat
        Calendar cal = Calendar.getInstance();
        DateFormat dateFormat = new SimpleDateFormat("yyyyMMdd");

        //note cari folder pada server untuk hari ini
        //gunanya kalo perpindahan bulan juga akan masuk ke folder bulan lalu
        Calendar cal2 = Calendar.getInstance();
        DateFormat dateFormat2 = new SimpleDateFormat("yyyyMM");

        String hostName = setting.getFtpHost();
        String username = setting.getFtpUsername();
        String password = setting.getFtpPassword();
        Integer port = Integer.parseInt(setting.getFtpPort());
        System.out.println("Host:" + hostName);
        System.out.println("User N Pass ::" + username + "," + password);
        System.out.println("Port::" + port);
        //String location = "/Prepaid/201211/FTR";
        //String location = "/home/adji/PELANGI/PELANGI-INTAKA/FTRCreatorDirect/test.upload";
        //String location = "test.upload";

        FTPClient ftp = null;
        InputStream in = null;
        FTPClient ftpClient = new FTPClient();

        try {
            ftp = new FTPClient();
            ftp.addProtocolCommandListener(new PrintCommandListener(new PrintWriter(System.out)));
            int reply;
//            ftp.connect(hostName,port);
//    
//            ftp.login(username, password);

            ftpClient.connect(hostName, port);
            ftpClient.login(username, password);
            ftpClient.enterLocalPassiveMode();

            ftpClient.setFileType(FTP.BINARY_FILE_TYPE);

            logger.info("FTP account : " + hostName + " " + username + " " + password);
        } catch (SocketException ex) {
            logger.info("ERROR SOCKET ON CONNECT");
            Logger.getLogger(FTRCreatorDirect.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            logger.info("ERROR IO ON CONNECT");
            Logger.getLogger(FTRCreatorDirect.class.getName()).log(Level.SEVERE, null, ex);
        }

        for (int i = 0; i < type.length; i++) {
            try {
                String location = kode[i] + "-" + fileDate + ".ftr";
                String[] locationX = kode[i].split("-");
                String localLocation = locationX[0] + "/" + fileDate.substring(0, 6) + "/" + locationX[1] + "/";

                ftp.changeWorkingDirectory("/" + type[i] + "/" + fileDate.substring(0, 6) + "/FTR");

                int reply = ftp.getReplyCode();
                logger.info("Received Reply from FTP Connection:" + reply);
                logger.info("/" + type[i] + "/" + fileDate.substring(0, 6) + "/FTR/" + location);

                if (FTPReply.isPositiveCompletion(reply)) {
                    logger.info("Connected Success");
                }

                File f1 = new File(System.getProperty("user.dir") + "/" + localLocation + location);
                in = new FileInputStream(f1);

                ftp.storeFile(location, in);
                logger.info("SUCCESS " + type[i]);
            } catch (Exception e) {
                logger.info("ERROR UPLOAD " + type[i]);
                e.printStackTrace();
            }
        }

        try {
            ftp.logout();
            ftp.disconnect();
        } catch (Exception ex) {
            logger.info("ERROR LOGOUT");
        }

    }
}
