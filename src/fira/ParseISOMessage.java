package fira;
import java.io.IOException;
import java.util.Arrays;
import org.jpos.iso.*;

import org.jpos.iso.packager.*;
import java.util.HashMap;
import java.util.Map;

public class ParseISOMessage {

    public static void logISOMsg(ISOMsg msg, GenericPackager packager) {
        System.out.println("----ISO MESSAGE-----");
        try {
            System.out.println("  MTI : " + msg.getMTI());

            System.out.println("  Bitmap : " + msg.getValue(-1).toString());

            String bitmapString = msg.getValue(-1).toString();
            bitmapString = bitmapString.replace("}", "");
            bitmapString = bitmapString.replace("{", "");
            String[] //bitmapArray = { msg.getMTI() };
                    bitmapArray = bitmapString.split(",");

            for (int i = 0; i < bitmapArray.length; i++) {
                String string = bitmapArray[i].trim();
                System.out.println("bitmap " + i + " : " + packager.getFieldDescription(msg, Integer.parseInt(string)) + string.trim());
            }
            System.out.println("=====================================");

            System.out.println("  2 : " + msg.getString(2));
            System.out.println("  3 : " + msg.getString(3));
            System.out.println("  4 : " + msg.getString(4));
            System.out.println("  5 : " + msg.getString(5));
            System.out.println("  6 : " + msg.getString(6));
            System.out.println("  7 : " + msg.getString(7));
            System.out.println("  11 : " + msg.getString(11));
            System.out.println("  12 : " + msg.getString(12));
            System.out.println("  13 : " + msg.getString(13));
            System.out.println("  15 : " + msg.getString(15));
            System.out.println("  18 : " + msg.getString(18));
            System.out.println("  26 : " + msg.getString(26));
            System.out.println("  32 : " + msg.getString(32));
            System.out.println("  33 : " + msg.getString(33));
            System.out.println("  37 : " + msg.getString(37));
            System.out.println("  39 : " + msg.getString(39));
            System.out.println("  40 : " + msg.getString(40));
            System.out.println("  41 : " + msg.getString(41));
            System.out.println("  42 : " + msg.getString(42));
            System.out.println("  48 : " + msg.getString(48));
            System.out.println("  49 : " + msg.getString(49));
            System.out.println("  55 : " + msg.getString(55));
            System.out.println("  60 : " + msg.getString(60));
            System.out.println("  63 : " + msg.getString(63));
            System.out.println("  70 : " + msg.getString(70));
            System.out.println("  73 : " + msg.getString(73));
            System.out.println("  90 : " + msg.getString(90));

            // String[] iwan = parseBit48Inquiry(msg.getString(48));

            //System.out.println("iwan : "+iwan[3]);

        } catch (ISOException e) {
            e.printStackTrace();
        } finally {
            System.out.println("--------------------");
        }

    }

    public static String[] parseBit48Inquiry(String msg) {
        String[] hasil = new String[65];

        try {
            hasil[0] = msg.substring(0, 7);
            System.out.println("switcher id : " + hasil[0]);// switcher id - 7
            hasil[1] = msg.substring(7, 19);
            System.out.println("sub id : " + hasil[1]);// subscriber id - 12
            hasil[2] = String.valueOf(msg.charAt(19));
            System.out.println("bill status : " + hasil[2]); //bill status - 1
            hasil[3] = msg.substring(20, 22);
            System.out.println("outstanding bill : " + hasil[3]); //outstanding bill - 2
            hasil[4] = msg.substring(22, 54);
            System.out.println("switcher ref : " + hasil[4]); //switcher ref no - 32
            hasil[5] = msg.substring(54, 79);
            System.out.println("sub name : " + hasil[5]); //Subscriber name - 25
            hasil[6] = msg.substring(79, 84);
            System.out.println("service unit : " + hasil[6]); //Service unit - 5
            hasil[7] = msg.substring(84, 99);
            System.out.println("service unit phone : " + hasil[7]); //Service unit phone - 15
            hasil[8] = msg.substring(99, 103);
            System.out.println("sub segmentation : " + hasil[8]); //subscriber segmentation - 4
            hasil[9] = msg.substring(103, 112);
            System.out.println("power consuming cat : " + hasil[9]);  //Power consuming cat - 9
            hasil[10] = msg.substring(112, 121);
            System.out.println("total admin charge : " + hasil[10]); //Total admin charges - 9

            hasil[11] = msg.substring(121, 127);
            System.out.println("bill period : " + hasil[11]); //bill period - 6
            hasil[12] = msg.substring(127, 135);
            System.out.println("due date : " + hasil[12]); //due date - 8
            hasil[13] = msg.substring(135, 143);
            System.out.println("meter read date : " + hasil[13]); //meter read date - 8
            hasil[14] = msg.substring(143, 154);
            System.out.println("total electricity bill : " + hasil[14]); //total electricity bill - 11
            hasil[15] = msg.substring(154, 165);
            System.out.println("incentive : " + hasil[15]); //incentive - 11
            hasil[16] = msg.substring(165, 175);
            System.out.println("vat : " + hasil[16]); //vat - 10
            hasil[17] = msg.substring(175, 184);
            System.out.println("penalty fee : " + hasil[17]); //penalty fee - 9
            hasil[18] = msg.substring(184, 192);
            System.out.println("prev meter reading : " + hasil[18]); //prev meter reading 1 - 8
            hasil[19] = msg.substring(192, 200);
            System.out.println("curr meter reading : " + hasil[19]); //curr meter reading 1 - 8
            hasil[20] = msg.substring(200, 208);
            System.out.println("prev meter reading 2 : " + hasil[20]); //prev meter reading 2 - 8
            hasil[21] = msg.substring(208, 216);
            System.out.println("curr meter reading 2 : " + hasil[21]); //curr meter reading 2 - 8
            hasil[22] = msg.substring(216, 224);
            System.out.println("prev meter reading 3 : " + hasil[22]); //prev meter reading 3 - 8
            hasil[23] = msg.substring(224, 232);
            System.out.println("curr meter reading 3 : " + hasil[23]); //curr meter reading 3 - 8


            if (Integer.parseInt(hasil[2]) > 1) {
                hasil[24] = msg.substring(232, 238); //bill period - 6
                hasil[25] = msg.substring(238, 246); //due date - 8
                hasil[26] = msg.substring(246, 254); //meter read date - 8
                hasil[27] = msg.substring(254, 265); //total electricity bill - 11
                hasil[28] = msg.substring(265, 276); //incentive - 11
                hasil[29] = msg.substring(276, 286); //vat - 10
                hasil[30] = msg.substring(286, 295); //penalty fee - 9
                hasil[31] = msg.substring(295, 303); //prev meter reading 1 - 8
                hasil[32] = msg.substring(303, 311); //curr meter reading 1 - 8
                hasil[33] = msg.substring(311, 319); //prev meter reading 2 - 8
                hasil[34] = msg.substring(319, 327); //prev meter reading 2 - 8
                hasil[35] = msg.substring(327, 335); //prev meter reading 3 - 8
                hasil[36] = msg.substring(335, 343); //prev meter reading 3 - 8
            }

            if (Integer.parseInt(hasil[2]) > 2) {
                hasil[37] = msg.substring(343, 349);
                hasil[38] = msg.substring(349, 357);
                hasil[39] = msg.substring(357, 365);
                hasil[40] = msg.substring(365, 376);
                hasil[41] = msg.substring(376, 387);
                hasil[42] = msg.substring(387, 397);
                hasil[43] = msg.substring(397, 406);
                hasil[44] = msg.substring(406, 414);
                hasil[45] = msg.substring(414, 422);
                hasil[46] = msg.substring(422, 430);
                hasil[47] = msg.substring(430, 438);
                hasil[48] = msg.substring(438, 446);
                hasil[49] = msg.substring(446, 454);
            }

            if (Integer.parseInt(hasil[2]) > 3) {
                hasil[50] = msg.substring(454, 460);
                hasil[51] = msg.substring(460, 468);
                hasil[52] = msg.substring(468, 476);
                hasil[53] = msg.substring(476, 487);
                hasil[54] = msg.substring(487, 498);
                hasil[55] = msg.substring(498, 508);
                hasil[56] = msg.substring(508, 517);
                hasil[57] = msg.substring(517, 525);
                hasil[58] = msg.substring(525, 533);
                hasil[59] = msg.substring(533, 541);
                hasil[60] = msg.substring(541, 549);
                hasil[61] = msg.substring(549, 557);
                hasil[62] = msg.substring(557, 564);
            }


        } catch (Exception e) {
            e.printStackTrace();
        }

        return hasil;
    }

    public Map<String, String> getBitmap(String data) {
        System.out.println("=============== getting bitmap =========");
        Map<String, String> bitMap = new HashMap<String, String>();
        try {
            GenericPackager packager = new GenericPackager("packager/iso87ascii.xml");
//            GenericPackager packager = new GenericPackager("packager/isoSLSascii.xml");
//            GenericPackager newpackager = new GenericPackager("packager/iso87ascii_minipack.xml");
            ISOMsg isoMsg = new ISOMsg();
//            ISOMsg isoMsgNew = new ISOMsg();
//            isoMsgNew.setPackager(newpackager);
            
            isoMsg.setPackager(packager);
            isoMsg.unpack(data.getBytes());
            System.out.println("======> "+isoMsg.getValue(-1));
            String bitmapString = isoMsg.getValue(-1).toString();
            bitmapString = bitmapString.replace("}", "");
            bitmapString = bitmapString.replace("{", "");
            String[] bitmapArray = bitmapString.split(",");
            System.out.println("bitmapArray :: "+Arrays.toString(bitmapArray));
            System.out.println("=================DATA FIELD PARSING=====================");
            bitMap.put("MTI", isoMsg.getMTI());
            System.out.println("getting MTI from bitmap :: "+bitMap.get("MTI"));
            System.out.println("bitmapArray length :: "+bitmapArray.length);
//            isoMsgNew.setMTI(isoMsg.getMTI());
            for (int i = 0; i < bitmapArray.length; i++) {
                String bitId = bitmapArray[i].trim();
//                System.out.println("bit :: "+bitId+" "+isoMsg.getString(bitId));
                bitMap.put(bitId, isoMsg.getString(bitId));
                System.out.println("BIT " + bitId + " : " + bitMap.get(bitId));
//                isoMsgNew.set(bitId, bitMap.get(bitId));
//                System.out.println(Arrays.toString(bitMap));
            }
            
//            byte[] datax = isoMsgNew.pack();
//            System.out.println(":: NEW ISO :: "+new String(datax));
            
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println(e.getMessage());
        }
        
        return bitMap;
    }
    
    
    
//    public Map<String, String> getBitmap(String data) throws ISOException {
//        // Create Packager based on XML that contain DE type
//        Map<String, String> bitMap = new HashMap<String, String>();
//
//        GenericPackager packager = new GenericPackager("isoSLSascii.xml");
//
//        // Create ISO Message
//        ISOMsg isoMsg = new ISOMsg();
//        isoMsg.setPackager(packager);
//
//
//        isoMsg.unpack(data.getBytes());
//        //String fieldDescription = packager.getFieldDescription(isoMsg, 1);
//        // print the DE list
//        //logISOMsg(isoMsg);
//        // print the DE list
//        //logISOMsg(isoMsg);
//
//
//        //isoMsg.get
//        String bitmapString = isoMsg.getValue(-1).toString();
//        bitmapString = bitmapString.replace("}", "");
//        bitmapString = bitmapString.replace("{", "");
//        String[] bitmapArray = bitmapString.split(",");
//
//        for (int i = 0; i < bitmapArray.length; i++) {
//            String bitId = bitmapArray[i].trim();
//
//            bitMap.put(bitId, isoMsg.getString(bitId));
//            System.out.println("bitmap " + bitMap.get(bitId) + " : " + bitMap.get(bitId));
//        }
//        return bitMap;
//        //return bitmapArray;
//    }
}
