/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fira;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
//import ppob_pln_sls.Settings;

public class SLSLogger {

    //Settings setting = new Settings();

    /*public static void main(String x[]) {
     SLSLogger a = new SLSLogger();
     //a.logAction("lsdf","PPOB");
     a.recapPLN("ngonar", "99501");
     }*/
//    String folder
    public void logAction(String iso, String payment_type) {
        try {
            String data = iso + "\r\n";

            Calendar currentDate = Calendar.getInstance();
            SimpleDateFormat formatter = new SimpleDateFormat("yyyyMMdd");
            String tgl = formatter.format(currentDate.getTime());

            //File file =new File(setting.getSwitchingCid()+"-"+payment_type+"-"+tgl+".log");
            File file = new File(payment_type + "-" + tgl + ".log");

            //if file doesnt exists, then create it
            if (!file.exists()) {
                file.createNewFile();
            }

            //true = append file
            FileWriter fileWritter = new FileWriter(file.getName(), true);
            BufferedWriter bufferWritter = new BufferedWriter(fileWritter);
            bufferWritter.write(data);
            bufferWritter.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void printSumToFile(String iso, String partnerNick, String payment_type, String tgl) {
        String[] locationX = payment_type.split("-");
//        String location = locationX[0] + "/" + tgl.substring(0, 6) + "/" + locationX[1];
        String location = partnerNick + "4510208" + locationX[1] + "/sum" + "/" + tgl.substring(0, 6);
//        String location = "/home/"+partnerNick +"_"+locationX[1] + "/ftr" + "/" + tgl.substring(0, 6);
        System.err.println(location);
//System.out.println(location);

        File folder = null;
//        String[] location2x = location.split("\\/");
//        String step = "/home/";
////        step = "";
//        System.err.println(System.getProperty("/home"));
//        for (int i = 0; i < location2x.length; i++) {
//            step += location2x[i]+"/";
//            System.out.println(step);
        folder = new File(location);
        if (folder.exists() && folder.isDirectory()) {
        } else {
            System.out.println("mkdir");
            try {
                folder.mkdirs();
            } catch (Exception e) {
                e.printStackTrace();
                e.getMessage();
            }
            System.out.println("done");
        }
//        }
        //folder = new File(System.getProperty("user.dir")+"/"+location);

        //location = locationX[0] + "/" + tgl.substring(0, 6) + "/" + locationX[1] + "/";
        try {
            //setting.setConnections();

//            String header = String.format("%-14s", "ID") +
//                    String.format("%-8s", "|CA_ID") +
//                    String.format("%-5s", "|CENTRAL_ID") +
//                    String.format("%-27s", "|MERCHANT")+
//                    String.format("%-33s", "|SREFNUM") +
//                    String.format("%-12s", "|IDPEL") +
//                    String.format("%-13s", "|TRAN_AMOUNT") +
//                    String.format("%-11s", "|ADM_CHARGE") +
//                    String.format("%-11s", "|STAMP_DUTY") +
//                    String.format("%-11s", "|VAT_TAX") +
//                    String.format("%-11s", "|LIGHT_TAX") +
//                    String.format("%-11s", "|INSTALLMENT") +
//                    String.format("%-13s", "|POWER_PURCHASE") +
//                    String.format("%-11s", "|PURCHASE_KWH") +
//                    String.format("%-16s", "|TOKEN") +
//                    String.format("%-8s", "|BANK_CODE") +
//                    String.format("%-17s", "|TERMINAL_ID")
//                    + "\r\n";
//
            String header = "ID"
                    + "|CA_ID"
                    + "|CENTRAL_ID"
                    + "|MERCHANT"
                    + "|SREFNUM"
                    + "|IDPEL"
                    + "|TRAN_AMOUNT"
                    + "|ADM_CHARGE"
                    + "|STAMP_DUTY"
                    + "|VAT_TAX"
                    + "|LIGHT_TAX"
                    + "|INSTALLMENT"
                    + "|POWER_PURCHASE"
                    + "|PURCHASE_KWH"
                    + "|TOKEN"
                    + "|BANK_CODE"
                    + "|TERMINAL_ID"
                    + "\r\n";

            String space[] = {"%40s", "%10s", "%32s", "%32s", "%32s"};
            header = ""
                    + String.format(space[0], "")//cid
                    + String.format(space[1], "") //product_id
                    + String.format(space[2], "")//trx_id
                    + String.format(space[3], "") //stan
                    + String.format(space[4], "")
                    + "\n";

            header = "";
            String data = iso;

//            Calendar currentDate = Calendar.getInstance();
//            SimpleDateFormat formatter= new SimpleDateFormat("yyyyMMdd");
//            String tgl = formatter.format(currentDate.getTime());
            //File file =new File(setting.getSwitchingCid()+"-"+payment_type+"-"+tgl+".ftr");
//            File file =new File(payment_type+"-"+tgl+".ftr");
//            File file = new File(folder,payment_type + "-" + tgl + ".ftr");
            String kodeLayananPln = "";
            if (locationX[0].toLowerCase().equalsIgnoreCase("plnpostpaid")) {
                kodeLayananPln = "XX501";
            } else if (locationX[0].toLowerCase().equalsIgnoreCase("plnprepaid")) {
                kodeLayananPln = "XX502";
            } else if (locationX[0].toLowerCase().equalsIgnoreCase("plnnontaglis")) {
                kodeLayananPln = "XX504";
            }

            String kodeBank = "037";
            String fileNameToCreate = "A3-SUM" + kodeBank + "-" + kodeLayananPln + "-" + "4510208" + "-" + locationX[2] + "-" + tgl + ".txt";
//note aslinya
//            File file = new File(folder, payment_type + "-" + tgl + "-SUM.txt");
            File file = new File(folder, fileNameToCreate);
//A3-SUM451-XX501-4510206-20141105.txt
            //if file doesnt exists, then create it
            if (!file.exists()) {
                file.createNewFile();
                FileWriter fileWritter = new FileWriter(file, true);
                BufferedWriter bufferWritter = new BufferedWriter(fileWritter);
                bufferWritter.write(header);
                bufferWritter.close();
            }

            //true = append file
            FileWriter fileWritter = new FileWriter(file, true);
            BufferedWriter bufferWritter = new BufferedWriter(fileWritter);
            bufferWritter.write(data);
            bufferWritter.close();

        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    
    public void printToFile(String iso, String partnerNick, String payment_type, String tgl,String ca) {
        String[] locationX = payment_type.split("-");
        //path folder 
        String location = "REKON" + "/" + tgl.substring(0, 6);


            File folder = null;
            folder = new File(location);
            if (folder.exists() && folder.isDirectory()) {
            } else {
                System.out.println("mkdir");
                try {
                    folder.mkdirs();
                } catch (Exception e) {
                    e.printStackTrace();
                    e.getMessage();
                }
                System.out.println("done");
            }

        try {

            

           String header = String.format("%-8s", "DATE TRX").trim() + "; "                  
                    + String.format("%-6s", "TIME TRX").trim() + "; " 
                    + String.format("%-13s", "CustomerID").trim() + "; " 
                    + String.format("%-6s", "KODE CA").trim() + "; " 
                    + String.format("%-6s", "KODE SUBCA").trim() + "; " 
                    + String.format("%-16s", "KODE PRODUK").trim() + "; " 
                    + String.format("%-10s", "AMOUNT").trim() + "; " 
                    + String.format("%-16s", "NOREFF").trim() + "; "
                    + String.format("%-16s", "ADD INFO / NAMA").trim() 
                    + "\r\n";

            String data = iso;
            //extension file
            File file = new File(folder, payment_type + "_"+ca+"_" + tgl + ".txt");

            //if file doesnt exists, then create it
            if (!file.exists()) {
                file.createNewFile();
                FileWriter fileWritter = new FileWriter(file, true);
                BufferedWriter bufferWritter = new BufferedWriter(fileWritter);
                bufferWritter.write(header);
                bufferWritter.close();
            }

            //true = append file
            FileWriter fileWritter = new FileWriter(file, true);
            BufferedWriter bufferWritter = new BufferedWriter(fileWritter);
            bufferWritter.write(data);
            bufferWritter.close();

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void printSumPusatToFile_v1(String iso, String partnerNick, String payment_type, String tgl) {
        String[] locationX = payment_type.split("-");
//        String location = locationX[0] + "/" + tgl.substring(0, 6) + "/" + locationX[1];
//        String location = partnerNick + "_" + locationX[1] + "/sum" + "/" + tgl.substring(0, 6);
        String location = "BSM/sum" + "/" + tgl.substring(0, 6);
//        String location = "/home/"+partnerNick +"_"+locationX[1] + "/ftr" + "/" + tgl.substring(0, 6);
        System.err.println(location);
//System.out.println(location);

        File folder = null;
//        String[] location2x = location.split("\\/");
//        String step = "/home/";
////        step = "";
//        System.err.println(System.getProperty("/home"));
//        for (int i = 0; i < location2x.length; i++) {
//            step += location2x[i]+"/";
//            System.out.println(step);
        folder = new File(location);
        if (folder.exists() && folder.isDirectory()) {
        } else {
            System.out.println("mkdir");
            try {
                folder.mkdirs();
            } catch (Exception e) {
                e.printStackTrace();
                e.getMessage();
            }
            System.out.println("done");
        }
//        }
        //folder = new File(System.getProperty("user.dir")+"/"+location);

        //location = locationX[0] + "/" + tgl.substring(0, 6) + "/" + locationX[1] + "/";
        try {
            //setting.setConnections();

//            String header = String.format("%-14s", "ID") +
//                    String.format("%-8s", "|CA_ID") +
//                    String.format("%-5s", "|CENTRAL_ID") +
//                    String.format("%-27s", "|MERCHANT")+
//                    String.format("%-33s", "|SREFNUM") +
//                    String.format("%-12s", "|IDPEL") +
//                    String.format("%-13s", "|TRAN_AMOUNT") +
//                    String.format("%-11s", "|ADM_CHARGE") +
//                    String.format("%-11s", "|STAMP_DUTY") +
//                    String.format("%-11s", "|VAT_TAX") +
//                    String.format("%-11s", "|LIGHT_TAX") +
//                    String.format("%-11s", "|INSTALLMENT") +
//                    String.format("%-13s", "|POWER_PURCHASE") +
//                    String.format("%-11s", "|PURCHASE_KWH") +
//                    String.format("%-16s", "|TOKEN") +
//                    String.format("%-8s", "|BANK_CODE") +
//                    String.format("%-17s", "|TERMINAL_ID")
//                    + "\r\n";
//
            String header = "ID"
                    + "|CA_ID"
                    + "|CENTRAL_ID"
                    + "|MERCHANT"
                    + "|SREFNUM"
                    + "|IDPEL"
                    + "|TRAN_AMOUNT"
                    + "|ADM_CHARGE"
                    + "|STAMP_DUTY"
                    + "|VAT_TAX"
                    + "|LIGHT_TAX"
                    + "|INSTALLMENT"
                    + "|POWER_PURCHASE"
                    + "|PURCHASE_KWH"
                    + "|TOKEN"
                    + "|BANK_CODE"
                    + "|TERMINAL_ID"
                    + "\r\n";

            String space[] = {"%40s", "%10s", "%32s", "%32s", "%32s"};
            header = ""
                    + String.format(space[0], "")//cid
                    + String.format(space[1], "") //product_id
                    + String.format(space[2], "")//trx_id
                    + String.format(space[3], "") //stan
                    + String.format(space[4], "")
                    + "\n";

            header = "";
            String data = iso;

//            Calendar currentDate = Calendar.getInstance();
//            SimpleDateFormat formatter= new SimpleDateFormat("yyyyMMdd");
//            String tgl = formatter.format(currentDate.getTime());
            //File file =new File(setting.getSwitchingCid()+"-"+payment_type+"-"+tgl+".ftr");
//            File file =new File(payment_type+"-"+tgl+".ftr");
//            File file = new File(folder,payment_type + "-" + tgl + ".ftr");
            String kodeLayananPln = "";
            if (locationX[0].toLowerCase().equalsIgnoreCase("plnpostpaid")) {
                kodeLayananPln = "XX501";
            } else if (locationX[0].toLowerCase().equalsIgnoreCase("plnprepaid")) {
                kodeLayananPln = "XX502";
            } else if (locationX[0].toLowerCase().equalsIgnoreCase("plnnontaglis")) {
                kodeLayananPln = "XX504";
            }

            String kodeBank = "037";
            String fileNameToCreate = "A3-SUM" + kodeBank + "PV2-" + kodeLayananPln + "-" + locationX[1] + "-" + locationX[2] + "-" + tgl + ".txt";
//note aslinya
//            File file = new File(folder, payment_type + "-" + tgl + "-SUM.txt");
            File file = new File(folder, fileNameToCreate);
//A3-SUM451-XX501-4510206-20141105.txt
            //if file doesnt exists, then create it
//            if (!file.exists()) {
//                file.createNewFile();
//                FileWriter fileWritter = new FileWriter(file, true);
//                BufferedWriter bufferWritter = new BufferedWriter(fileWritter);
//                bufferWritter.write(header);
//                bufferWritter.close();
//            }

            if (!file.exists()) {
                //true = append file
                FileWriter fileWritter = new FileWriter(file, true);
                BufferedWriter bufferWritter = new BufferedWriter(fileWritter);
                bufferWritter.write(data);
                bufferWritter.close();
            }else if (file.exists() && !data.contains("MITRA") && !data.contains("=====")) {
                //true = append file
                FileWriter fileWritter = new FileWriter(file, true);
                BufferedWriter bufferWritter = new BufferedWriter(fileWritter);
                bufferWritter.write(data);
                bufferWritter.close();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    
    public void recapPLNPrepaid(String iso, String payment_type, String tgl) {
        String[] locationX = payment_type.split("-");
        String location = locationX[0] + "/" + tgl.substring(0, 6) + "/" + locationX[1];

        File folder = new File(location);
        if (folder.exists() && folder.isDirectory()) {
        } else {
            System.out.println("mkdir");
            folder.mkdirs();
            System.out.println("done");
        }
        folder = new File(System.getProperty("user.dir") + "/" + location);

        location = locationX[0] + "/" + tgl.substring(0, 6) + "/" + locationX[1] + "/";
        try {
            //setting.setConnections();

//            String header = String.format("%-14s", "ID") +
//                    String.format("%-8s", "|CA_ID") +
//                    String.format("%-5s", "|CENTRAL_ID") +
//                    String.format("%-27s", "|MERCHANT")+
//                    String.format("%-33s", "|SREFNUM") +
//                    String.format("%-12s", "|IDPEL") +
//                    String.format("%-13s", "|TRAN_AMOUNT") +
//                    String.format("%-11s", "|ADM_CHARGE") +
//                    String.format("%-11s", "|STAMP_DUTY") +
//                    String.format("%-11s", "|VAT_TAX") +
//                    String.format("%-11s", "|LIGHT_TAX") +
//                    String.format("%-11s", "|INSTALLMENT") +
//                    String.format("%-13s", "|POWER_PURCHASE") +
//                    String.format("%-11s", "|PURCHASE_KWH") +
//                    String.format("%-16s", "|TOKEN") +
//                    String.format("%-8s", "|BANK_CODE") +
//                    String.format("%-17s", "|TERMINAL_ID")
//                    + "\r\n";
//
            String header = "ID"
                    + "|CA_ID"
                    + "|CENTRAL_ID"
                    + "|MERCHANT"
                    + "|SREFNUM"
                    + "|IDPEL"
                    + "|TRAN_AMOUNT"
                    + "|ADM_CHARGE"
                    + "|STAMP_DUTY"
                    + "|VAT_TAX"
                    + "|LIGHT_TAX"
                    + "|INSTALLMENT"
                    + "|POWER_PURCHASE"
                    + "|PURCHASE_KWH"
                    + "|TOKEN"
                    + "|BANK_CODE"
                    + "|TERMINAL_ID"
                    + "\r\n";

            String data = iso;

//            Calendar currentDate = Calendar.getInstance();
//            SimpleDateFormat formatter= new SimpleDateFormat("yyyyMMdd");
//            String tgl = formatter.format(currentDate.getTime());
            //File file =new File(setting.getSwitchingCid()+"-"+payment_type+"-"+tgl+".ftr");
//            File file =new File(payment_type+"-"+tgl+".ftr");
            File file = new File(folder, payment_type + "-" + tgl + ".ftr");

            //if file doesnt exists, then create it
            if (!file.exists()) {
                file.createNewFile();
                FileWriter fileWritter = new FileWriter(file, true);
                BufferedWriter bufferWritter = new BufferedWriter(fileWritter);
                bufferWritter.write(header);
                bufferWritter.close();
            }

            //true = append file
            FileWriter fileWritter = new FileWriter(file, true);
            BufferedWriter bufferWritter = new BufferedWriter(fileWritter);
            bufferWritter.write(data);
            bufferWritter.close();

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void recapPLN(String iso, String payment_type, String tgl) {
        String[] locationX = payment_type.split("-");
        String location = locationX[0] + "/" + tgl.substring(0, 6) + "/" + locationX[1];

        File folder = new File(location);
        if (folder.exists() && folder.isDirectory()) {
        } else {
            System.out.println("mkdir");
            folder.mkdirs();
            System.out.println("done");
        }

        folder = new File(System.getProperty("user.dir") + "/" + location);
        location = locationX[0] + "/" + tgl.substring(0, 6) + "/" + locationX[1] + "/";
        try {
            //setting.setConnections();

//            String header = String.format("%-14s", "ID") +
//                    String.format("%-8s", "|CA_ID") +
//                    String.format("%-8s", "|CENTRAL_ID") +
//                    String.format("%-5s", "|MERCHANT")+
//                    String.format("%-26s", "|SREFNUM") +
//                    String.format("%-13s", "|IDPEL") +
//                    String.format("%-7s", "|BILL_PERIOD") +
//                    String.format("%-13s", "|TRAN_AMOUNT") +
//                    String.format("%-12s", "|TOTAL_BILL") +
//                    String.format("%-7s", "|BILL") +
//                    String.format("%-12s", "|RP_INSENTIF") +
//                    String.format("%-11s", "|VAT") +
//                    String.format("%-10s", "|PENALTY") +
//                    String.format("%-10s", "|ADM_CHARGE") +
//                    String.format("%-17s", "|TERMINAL_ID")
//                    + "\r\n";
            String header = "ID"
                    + "|CA_ID"
                    + "|CENTRAL_ID"
                    + "|MERCHANT"
                    + "|SREFNUM"
                    + "|IDPEL"
                    + "|BILL_PERIOD"
                    + "|TRAN_AMOUNT"
                    + "|TOTAL_BILL"
                    + "|BILL"
                    + "|RP_INSENTIF"
                    + "|VAT"
                    + "|PENALTY"
                    + "|ADM_CHARGE"
                    + "|TERMINAL_ID"
                    + "\r\n";

            //String data = iso+"\r\n";
            String data = iso;

//            Calendar currentDate = Calendar.getInstance();
//            SimpleDateFormat formatter= new SimpleDateFormat("yyyyMMdd");
//            String tgl = formatter.format(currentDate.getTime());
            File file = new File(folder, payment_type + "-" + tgl + ".ftr");

            //if file doesnt exists, then create it
            if (!file.exists()) {
                file.createNewFile();
                FileWriter fileWritter = new FileWriter(file, true);
                BufferedWriter bufferWritter = new BufferedWriter(fileWritter);
                bufferWritter.write(header);
                bufferWritter.close();
            }

            //true = append file
            FileWriter fileWritter = new FileWriter(file, true);
            BufferedWriter bufferWritter = new BufferedWriter(fileWritter);
            bufferWritter.write(data);
            bufferWritter.close();

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void recapPLNNon(String iso, String payment_type, String tgl) {
        String[] locationX = payment_type.split("-");
        String location = locationX[0] + "/" + tgl.substring(0, 6) + "/" + locationX[1];

        File folder = new File(location);
        if (folder.exists() && folder.isDirectory()) {
        } else {
            System.out.println("mkdir");
            folder.mkdirs();
            System.out.println("done");
        }

        folder = new File(System.getProperty("user.dir") + "/" + location);
        location = locationX[0] + "/" + tgl.substring(0, 6) + "/" + locationX[1] + "/";
        try {
            //setting.setConnections();

//            String header = String.format("%-14s", "DT")
//                   + String.format("%-8s", "|CID")
//                   + String.format("%-4s", "|MERCHANT")
//                   + String.format("%-29s", "|REFNUM")
//                   + String.format("%-33s", "|SREFNUM")
//                   + String.format("%-13s", "|SUBID")
//                   + String.format("%-14s", "|REGNUM")
//                   + String.format("%-9s", "|REGD")
//                   + String.format("%-4s", "|TRAN_CODE")
//                   + String.format("%-12s", "|TOTAL_AMOUNT")
//                   + String.format("%-17s", "|TRAN_AMOUNT")
//                   + String.format("%-11s", "|ADMIN_CHARGES")
//                   + String.format("%-8s", "|BANKCODE")
//                   + String.format("%-16s", "|PPID")
//                   + "\r\n";
            String header = "DT"
                    + "|CID"
                    + "|MERCHANT"
                    + "|REFNUM"
                    + "|SREFNUM"
                    + "|SUBID"
                    + "|REGNUM"
                    + "|REGD"
                    + "|TRAN_CODE"
                    + "|TOTAL_AMOUNT"
                    + "|TRAN_AMOUNT"
                    + "|ADMIN_CHARGES"
                    + "|BANKCODE"
                    + "|PPID"
                    + "\n";

            //String data = iso+"\r\n";
            String data = iso;

//            Calendar currentDate = Calendar.getInstance();
//            SimpleDateFormat formatter= new SimpleDateFormat("yyyyMMdd");
//            String tgl = formatter.format(currentDate.getTime());
            File file = new File(folder, payment_type + "-" + tgl + ".ftr");

            //if file doesnt exists, then create it
            if (!file.exists()) {
                file.createNewFile();
                FileWriter fileWritter = new FileWriter(file, true);
                BufferedWriter bufferWritter = new BufferedWriter(fileWritter);
                bufferWritter.write(header);
                bufferWritter.close();
            }

            //true = append file
            FileWriter fileWritter = new FileWriter(file, true);
            BufferedWriter bufferWritter = new BufferedWriter(fileWritter);
            bufferWritter.write(data);
            bufferWritter.close();

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void infoRecapPLN(String iso, String payment_type, String tgl) {
        try {
            //setting.setConnections();

//            String header = String.format("%-14s", "ID") +
//                    String.format("%-8s", "|CA_ID") +
//                    String.format("%-5s", "|CENTRAL_ID") +
//                    String.format("%-27s", "|MERCHANT")+
//                    String.format("%-33s", "|SREFNUM") +
//                    String.format("%-12s", "|IDPEL") +
//                    String.format("%-13s", "|TRAN_AMOUNT") +
//                    String.format("%-11s", "|ADM_CHARGE") +
//                    String.format("%-11s", "|STAMP_DUTY") +
//                    String.format("%-11s", "|VAT_TAX") +
//                    String.format("%-11s", "|LIGHT_TAX") +
//                    String.format("%-11s", "|INSTALLMENT") +
//                    String.format("%-13s", "|POWER_PURCHASE") +
//                    String.format("%-11s", "|PURCHASE_KWH") +
//                    String.format("%-16s", "|TOKEN") +
//                    String.format("%-8s", "|BANK_CODE") +
//                    String.format("%-17s", "|TERMINAL_ID")
//                    + "\r\n";
//
            String header = "\r\n";

            String data = iso;

//            File file =new File(payment_type+"-"+tgl+".ftr");
            File file = new File("pln_transaction_info" + "-" + tgl + ".txt");

            //if file doesnt exists, then create it
            if (!file.exists()) {
                file.createNewFile();
                FileWriter fileWritter = new FileWriter(file.getName(), true);
                BufferedWriter bufferWritter = new BufferedWriter(fileWritter);
                bufferWritter.write(header);
                bufferWritter.close();
            }

            //true = append file
            FileWriter fileWritter = new FileWriter(file.getName(), true);
            BufferedWriter bufferWritter = new BufferedWriter(fileWritter);
            bufferWritter.write(data);
            bufferWritter.close();

        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
